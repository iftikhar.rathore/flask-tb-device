from . import onboarding
from . import telemetry
from . import mqtt
from . import ota

def init_app(app):
    mqtt.init_app(app)
    ota.init_app(app)
    telemetry.init_app(app)
    onboarding.init_app(app)



