
import flask
from tb_device_mqtt import TBDeviceMqttClient, FW_TITLE_ATTR, FW_VERSION_ATTR

def init_app(app:flask.Flask):
    client: TBDeviceMqttClient = app.extensions.get('tb_mqtt_client')
    if not client or not client.is_connected():
        return "Not connected to MQTT"
    firmware_version = '2.0' # app.extensions.get('version','v0')
    firmware_title = 'dummy_update'
    client.current_firmware_info = {
            "current_" + FW_TITLE_ATTR: firmware_title,
            "current_" + FW_VERSION_ATTR: firmware_version
    }
    client.get_firmware_update()