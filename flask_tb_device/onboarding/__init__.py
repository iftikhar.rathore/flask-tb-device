from . import routes
import flask

def init_app(app:flask.Flask):
    app.register_blueprint(routes.bp)