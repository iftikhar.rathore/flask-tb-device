from . import endpoints
import flask

def init_app(app:flask.Flask):
    return
    app.register_blueprint(endpoints.bp)