# -*- coding: utf-8 -*-
import locale
import flask_tb_device
import flask
import logging
from pathlib import Path
import logging

#logging.basicConfig(filename="/tmp/app.log", level=logging.INFO, filemode="w", format="%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s")

def create_app():
    instance_path = Path(__file__).parent / 'instance'
    app = flask.Flask(__name__,instance_path=instance_path,instance_relative_config=True,static_folder=None)

    app.config.from_pyfile('settings.py')
    app.extensions['version'] = get_version(app)

    flask_tb_device.init_app(app)

    client = app.extensions['tb_mqtt_client']

    @client.on_rpc('restart')
    def restart(wait_ms='0'):
        print(f'restarting in {wait_ms} milis')

    return app

def get_version(app:flask.Flask):
    with open(Path(app.root_path) / '__version__.py') as f:
        return f.read()

if __name__ == '__main__':

    app = create_app()
    
    print(app.root_path)
    print(get_version(app))
    app.run()




