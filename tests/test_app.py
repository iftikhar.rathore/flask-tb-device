import flask_tb_device
from pathlib import Path
import flask

HERE = Path(__file__).parent
EXAMPLES = HERE.parent / "examples"
TEST_INSTANCE = EXAMPLES / "instance"

def create_app(instance_path=None):

    app = flask.Flask(__name__,instance_path=instance_path)

 #   app.extensions['version'] = get_version(app)

    flask_tb_device.init_app(app)
    return app

def test_app():
    app = create_app(TEST_INSTANCE)
    client = app.test_client()
    response = client.get("/claim")
    assert app.config["dev_name"] in response.data.decode()
