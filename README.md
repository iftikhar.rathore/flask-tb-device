<!--| p(f'# {values.project.name}') |-->
# Flask-TB-Device
<!--|end|-->

<!--| p(f'# {values.project.description}') |-->
# Flask Thingboard Extensins
<!--|end|-->

![flask-tb-dev](docs/src/images/flask-tb-dev.png)
<!---|START introduction|--->
## Introduction

**Flask-TB-device is a Flask based project which provided necessary extension to Thingsboard. I allows you to easily create applications for Thingsboard framwork without requiring inner knowledge of the framework**

Flask-TB-Device implements following extensions
* [OTA](#1-OTA) 

* [Onboarding/Claiming](#2-claim) 

* [RPC](#3-RPC) 

* [Telemetry](#telemetry) 

## Requirements
Flask-TB-Device requires python version 3, it is no longer supported on python version 2.

## Installation

The Flask-TB-Device extension can be installed using pip

````bash
pip install flask-tb-device
```` 
<!---|END introduction|--->

### 1. OTA
The Over-The-Air update (OTA) provides functionality for downloading and installing firmware/software updates. It provides multiple mechanisms to allow customization of the download and installation process.

* Simple download and install
This mechanism provides a simple but fixed mechanism to download and install the update file. The user needs to set the version and initialize the mechanism. The extension will automatically download if a higher version of the software is available.

````python
  import flask_tb_device
    flask_tb_device.init_app(app)
    
````





